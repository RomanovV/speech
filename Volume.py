#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from xml.dom.minidom import parse, parseString
from libary import vlc
import struct
import math
import urllib2
import uuid
import pyaudio
import wave
import logging
import urllib
import time

KEY = 'c5ba9212-030d-463c-b2a0-868cb349373f'
CHUNK = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 16000
RECORD_SECONDS = 3
WAVE_OUTPUT_FILENAME = "sound/output.wav"
WAVE_START_FILENAME = "sound/start.wav"
INITIAL_TAP_THRESHOLD = 0.700
SHORT_NORMALIZE = (1.0 / 32768.0)
INPUT_BLOCK_TIME = 0.1
INPUT_FRAMES_PER_BLOCK = int(RATE * INPUT_BLOCK_TIME)
MAX_TAP_BLOCKS = 0.15 / INPUT_BLOCK_TIME


class Volume(object):
    def __init__(self):
        self.command = []
        self.text = ''
        self.run()

    def run(self):

        self.pull()
        self.send()
        self.parse()
        self.play()

    class ServerError(RuntimeError):
        def __init__(self, message):
            logging.basicConfig(filename='log.txt', level=logging.DEBUG,
                                format='%(asctime)s - %(levelname)s - %(message)s')
            logging.debug(message)

    def get_rms(self, block):

        count = len(block) / 2
        format = "%dh" % (count)
        shorts = struct.unpack(format, block)

        sum_squares = 0.0
        for sample in shorts:
            # sample is a signed short in +/- 32768.
            # normalize it to 1.0
            n = sample * SHORT_NORMALIZE
            sum_squares += n * n

        return math.sqrt(sum_squares / count)

    def pull(self):

        pa = pyaudio.PyAudio()

        stream = pa.open(format=FORMAT,
                         channels=CHANNELS,
                         rate=RATE,
                         input=True,
                         frames_per_buffer=INPUT_FRAMES_PER_BLOCK)

        tap_threshold = INITIAL_TAP_THRESHOLD

        stream = self.getStream(stream)
        for i in stream:
            amplitude = self.get_rms(i)
            print amplitude
            if amplitude > tap_threshold:
                self.player(WAVE_START_FILENAME, 0.8)
                self.record()
                break

    def getStream(self, stream):
        while True:
            yield stream.read(INPUT_FRAMES_PER_BLOCK)

    def send(self):
        uid = uuid.uuid4().hex
        headers = {'Content-Type': 'audio/x-wav'}

        f = {
            'lang': "ru-RU",
            'topic': "notes",
            'uuid': str(uid),
            'key': KEY,
        }
        url = urllib.urlencode(f)
        url = "https://asr.yandex.net/asr_xml?" + url

        try:
            file = open(WAVE_OUTPUT_FILENAME, 'rb').read()
        except Exception as E:
            self.ServerError(E)

        try:
            request = urllib2.Request(url, data=file, headers=headers)
            response = urllib2.urlopen(request)

            self.command = response.read()

        except Exception as E:
            self.ServerError(E)

    def record(self):
        p = pyaudio.PyAudio()
        frames = []
        stream = p.open(format=FORMAT,
                        channels=CHANNELS,
                        rate=RATE,
                        input=True,
                        frames_per_buffer=CHUNK)

        print("* recording")

        for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
            data = stream.read(CHUNK)
            frames.append(data)

        print("* done recording")

        stream.stop_stream()
        stream.close()
        p.terminate()

        wf = wave.open(WAVE_OUTPUT_FILENAME, 'wb')
        wf.setnchannels(CHANNELS)
        wf.setsampwidth(p.get_sample_size(FORMAT))
        wf.setframerate(RATE)
        wf.writeframes(b''.join(frames))
        wf.close()

    def parse(self):
        if not self.command:
            self.ServerError('Нет xml')
            self.run()
            return

        command = self.command
        dom = parseString(command)

        success = int(dom.getElementsByTagName('recognitionResults')[0]._attrs['success'].value)
        if not success == 1:
            self.ServerError('Не распознано')
            self.text = u"Не распознана"
            self.play()
            self.run()
            return

        confidence = float(dom.getElementsByTagName('variant')[0]._attrs['confidence'].value)
        if confidence > 0:
            self.text = dom.getElementsByTagName('variant')[0].firstChild.data
            self.ServerError('confidence = ' + str(confidence) + ' text=' + self.text)

    def play(self):
        self.ssh()
        if not self.text:
            self.ServerError('Команда отсутствует')
            self.text = u"Не распознана"
            self.play()
            self.run()
            return

        try:
            text = u'Команда '
            f = {
                'text': text.encode('utf-8') + self.text.encode('utf-8'),
                'format': "wav",
                'lang': "ru-RU",
                'speaker': "jane",
                'emotion': "good",
                'key': KEY,
            }
            url = urllib.urlencode(f)
            self.player("https://tts.voicetech.yandex.net/generate?" + url)
            self.run()




        except Exception as E:
            self.ServerError(E)

    def player(self, url, sleep=0):

        p = vlc.MediaPlayer(url)
        p.play()
        time.sleep(sleep)

    def ssh(self):
        import subprocess
        import sys

        HOST="root@192.0.0.39"
        # Ports are handled in ~/.ssh/config since we use OpenSSH
        COMMAND='mosquitto_pub -t "/devices/noolite_tx_0x871/controls/state/on" -m "1"'

        ssh = subprocess.Popen(["ssh", "%s" % HOST, COMMAND],
                               shell=False,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
        result = ssh.stdout.readlines()
        if result == []:
            error = ssh.stderr.readlines()
            print >>sys.stderr, "ERROR: %s" % error
        else:
            print result



if __name__ == "__main__":
    Volume()
else:
    Volume()
